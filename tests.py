from kata import Game
from unittest import TestCase


class GameTestCase(TestCase):
    def setUp(self):
        self.game = Game()

    def test_add_point_p1(self):
        self.game.add_point(1)
        self.assertEqual(self.game.points[1], 1)
        self.game.add_point(1)
        self.game.add_point(1)
        self.assertEqual(self.game.points[1], 3)

    def test_add_point_p2(self):
        self.game.add_point(2)
        self.assertEqual(self.game.points[2], 1)

    def test_p1wins(self):
        self.game.add_point(1)
        self.game.add_point(1)
        self.game.add_point(1)
        self.assertEqual(self.game.finished, True)
        self.assertEqual(self.game.score(), "40 - 0")

    def test_p2wins(self):
        self.game.add_point(2)
        self.game.add_point(2)
        self.game.add_point(2)
        self.assertEqual(self.game.finished, True)
        self.assertEqual(self.game.score(), "0 - 40")

    def test_p1_score(self):
        self.game.add_point(1)
        self.assertEqual(self.game.score(1), "15")

    def test_p2_score(self):
        self.game.add_point(2)
        self.assertEqual(self.game.score(2), "15")

    def test_game_score(self):
        self.game.add_point(1)
        self.game.add_point(2)
        self.assertEqual(self.game.score(), "15 - 15")
        self.game.add_point(1)
        self.game.add_point(2)
        self.assertEqual(self.game.score(), "30 - 30")
        self.game.add_point(1)
        self.assertEqual(self.game.score(), "40 - 30")
